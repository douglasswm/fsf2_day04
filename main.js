var express = require("express");
var app = express();

app.use(express.static(__dirname + "/public"));

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);
app.listen(app.get("port"), function(){
    console.info("application started on port %d", app.get("port"));
});